<?php

require_once "core/init.php";
require_once "view/header.php";


$articles = tampilkan();

$admin = false;
$login = false;
if (isset($_SESSION['Username'])) {
    $login = true;
    if (status($_SESSION['Username']) == 0) {
        $admin = true;
    }
};

?>



<div class="jumbotron bg-transparent">
    <div class="container">
        <h1 class="display-4 m-auto text-jumbotron font-weight-bold">MENULIS ADALAH UPAYA
            UNTUK MERAWAT
            INGATAN</h1>
        <div class="scroll-icon text-center">
            <a class="nav-link" href="#scrolldown"><img src="view/asset/icon/scroll_down.svg"></a>
        </div>
    </div>

    <section class="section-content">
        <div class="container mt-5">
            <div class="row">
                <?php while ($row =  mysqli_fetch_assoc($articles)) : ?>
                    <div class="col-12 mt-5">




                        <div class="title text-center" id="scrolldown">
                            <img src="view/asset/icon/memphiz_title.svg" alt="" srcset="">
                            <h4><?= $row['title'] ?></h4>
                            <p><?php

                                $d = strtotime($row['time']);

                                echo date('l, y-m-d', $d) ?></p>
                            <hr>
                        </div>

                        <div class="content p-5 d-flex m-auto">
                            <p class="justify-content-center">
                                <?= $row['content'] ?>
                            </p>
                        </div>




                    </div>
                    <?php if ($login == true) : ?>
                        <div class="d-flex m-auto">
                            <a class="btn btn-primary mr-1" href="edit.php?id=<?= $row['id']; ?>">Edit</a>
                            <?php if ($admin == true) { ?>
                                <a class="btn btn-primary mr-1" href="delete.php?id=<?= $row['id']; ?>">delete</a>
                            <?php } ?>
                        </div>
                    <?php endif; ?>


                <?php endwhile;   ?>

            </div>
        </div>
    </section>




















    <?php require_once "view/footer.php"; ?>