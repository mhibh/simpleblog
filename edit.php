<?php

require_once "core/init.php";
require_once "view/header.php";

if (!isset($_SESSION['Username'])) {
    header('Location: login.php');
} else {

    $error = '';

    $id = $_GET['id'];


    if (isset($_GET['id'])) {
        $article = show_by($id);

        while ($row = mysqli_fetch_assoc($article)) {
            $title_id =  $row['title'];
            $content_id = $row['content'];
            $tag_id = $row['tag'];
        };
    };


    if (isset($_POST['submit'])) {

        $title = $_POST['title'];
        $content = $_POST['content'];
        $tag = $_POST['tag'];

        if (!empty(trim($title)) && !empty($content)) {

            if (edit_data($title, $content, $tag, $id)) {
                header('Location: index.php');
            } else {
                $error = 'ada masalah ketika edit data';
            };
        } else {
            $error =  'Form Wajid diisi';
        }
    };



?>



    <div class="container">

        <form action="" method="post">
            <div class="form-group">

                <label for="title">Judul</label>
                <input type="text" class="form-control" name="title" id="" value="<?= $title_id  ?>">

                <label for="content">Isi</label>
                <textarea type="text" class="form-control" name="content" id=""> <?= $content_id ?> </textarea>

                <label for="tag">Tag</label>
                <input type="text" class="form-control" name="tag" id="" value="<?= $tag_id  ?>">

                <div id="error"> <?= $error ?> </div>

                <input type="submit" name="submit" class="btn btn-primary mt-3" value="Submit">



            </div>

        </form>
    </div>









<?php require_once "view/footer.php";
} ?>