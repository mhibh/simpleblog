<?php

require_once "core/init.php";
require_once "view/header.php";

$error = '';

if (!isset($_SESSION['Username'])) {
    header('Location: login.php');
} else {


    if (isset($_POST['submit'])) {

        $title = $_POST['title'];
        $content = $_POST['content'];
        $tag = $_POST['tag'];

        if (!empty(trim($title)) && !empty($content)) {

            if (tambah_data($title, $content, $tag)) {
                header('Location: index.php');
            } else {
                $error = 'ada masalah ketika menambah data';
            };
        } else {
            $error =  'Form Wajid diisi';
        }
    }


?>



    <div class="container">

        <form action="" method="post">
            <div class="form-group">

                <label for="title">Judul</label>
                <input type="text" class="form-control" name="title" id="">

                <label for="content">Isi</label>
                <textarea type="text" class="form-control" name="content" id=""></textarea>

                <label for="tag">Tag</label>
                <input type="text" class="form-control" name="tag" id="">

                <div id="error"> <?= $error ?> </div>

                <input type="submit" name="submit" class="btn btn-primary mt-3" value="Submit">



            </div>

        </form>
    </div>









    <?php


    require_once "view/footer.php";

    ?>

<?php } ?>