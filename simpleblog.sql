-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 19 Des 2019 pada 15.36
-- Versi server: 10.4.8-MariaDB
-- Versi PHP: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `simpleblog`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `blog`
--

CREATE TABLE `blog` (
  `id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `content` longtext NOT NULL,
  `time` timestamp NOT NULL DEFAULT current_timestamp(),
  `tag` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `blog`
--

INSERT INTO `blog` (`id`, `title`, `content`, `time`, `tag`) VALUES
(11, 'Baim', 'sangat ganteng\r\n', '2019-12-19 12:26:01', ''),
(12, 'Merendahkan Status Influencer Hari Ini', 'Seorang wartawan sepuh Kompas, Andreas Maryoto sempat menulis sebuah curhatan serius. Sebuah curhatan berbentuk kolom yang menceritakan sejumlah rekan-rekannya yang bergerak dalam bidang kehumasan. Bukan karena kesal dengan klisenya hubungan media (wartawan) bersama orang-orang humas, melainkan me-repost curhatan rekan-rekannya itu tentang betapa repotnya ‘memanjakan’ para influencer.\r\n\r\nBeberapa cerita serupa juga sudah menyebar di dunia maya. Unggahan-unggahan tentang tidak mulusnya proses kerja sama oleh pebisnis dengan para influencer sudah bisa kita temukan. Para pebisnis mengeluhkan betapa manjanya seorang influencer itu. Dalam kolomnya, Maryoto menjabarkan, selain patokan harga yang seenak jidat, mereka juga minta yang macam-macam.\r\n\r\nPara pebisnis menyempatkan diri berperan sebagai warganet pada umumnya, mereka saling berbagi pengalaman: bagaimana jalinan ‘asmara’ mereka dengan para influencer itu. Kebanyakan, testimoni-testimoni yang akhirnya dikeluarkan itu berkesan buruk. Buruk karena penyewa jasa testimoni itu justru mengeluhkan orang-orang yang mereka bayar untuk mengeluarkan testimoni.\r\n\r\nDi era yang serba kekinian ini, menjadi seorang influencer merupakan sebuah cita-cita baru. Status tersebut sudah seperti sederajat dengan para selebritis. Hari ini, kita bisa melihat bahwa selebritas itu terbagi ke dua kutub yang berbeda, yaitu selebritis konvensional dan selebritis dunia maya.\r\n\r\nKata influencer ini sendiri mencuat karena dijadikan sebagai penanda buat orang-orang yang dikenal dan diikuti oleh banyak orang di sosial media. Untuk mendapatkan status tersebut, orang berlomba-lomba menjadi yang terdepan dengan koleksi jumlah pengikut alias followers yang amat sangat banyak.\r\n\r\nDengan semakin banyaknya orang-orang yang punya status ‘artis sosmed’ ini, tak pelak influencer berubah menjadi sebuah profesi baru. Pebisnis dan para marketer menggunakan mereka yang punya julukan tersebut untuk membantu kepentingan sebuah merek bisnis. Status itu menjelma sebagai ladang penghasil duit.\r\n\r\nDalam Kamus Cambridge, istilah itu kurang lebih bermakna: “orang atau kelompok yang memiliki kemampuan mempengaruhi opini atau perilaku orang lain”. Namun, ketika melihat dinamika dunia maya, influencer sedikit demi sedikit menemukan tanda-tanda perubahan. Seperti apa dan siapakah influencer itu sebenarnya?\r\n\r\nKalaupun definisi berpatok terus pada era kekinian seperti saat ini, kasihan sekali orang-orang yang akun sosmednya miskin followers. Kedengarannya tidak adil. Sosial media sepertinya memang bukanlah tempat untuk benar-benar memerdekakan diri dalam hal eksistensi. Bisa jadi, dunia itu adalah dunia virtual yang terang-terangan mengedepankan konsep hukum rimba gaya baru.\r\n\r\nSosial media awalnya diklaim sebagai tempat alternatif untuk bersosialisasi, justru beralih fungsi menjadi ajang unjuk diri. Demi jumlah pengikut serta status influencer, tidak jarang manusia-manusia sosmed kelas bawah alias ‘netizen biasa’ terjebak dalam rambu-rambu yang sarat akan persaingan. Menjadi terkenal, banyak pengikut, buka jasa promosi, disewa marketer, terus banyak duit, siapa yang tak mau?\r\n\r\nPerkembangan influencer sebenarnya sudah ada dari zaman kodok, hanya saja istilah ini makin sexy di era tsunami data saat ini. Definisinya pun menjadi lebih spesifik. Pemahaman bahwa istilah itu akan berlaku ketika sebuah ‘pengaruh’ hanya disebarkan melalui sosial media, agaknya lazim hari ini.\r\n\r\nMasalah tidak datang hanya pada pemahaman soal definisi pastinya saja, melainkan juga bagaimana istilah tersebut bekerja. Sebagaimana perkembangannya yang terlihat, status eksklusif itu memunculkan sebuah batasan khusus. Batas yang memisahkan netizen ke dalam dua kelas yang berbeda. Status yang merupakan sebagai penanda, sekaligus pembeda.\r\n\r\nMeski sama-sama pengguna, pada akhirnya sosmed seakan membagi antara mana yang influencer, dan mana yang non-influencer. Anggap saja, influencer adalah kelas atas sebagai puncak pimpinan yang membawahi kelas bawah. Walaupun tidak secara sistematis, kelas bawah yang diisi oleh kaum non-influencer itu terbawa suasana dan terpengaruh oleh daya pengaruh dari kelas yang berada di atasnya.\r\n\r\nNamun, batas ini tidak setegas sebuah struktur hierarkis. Sebagaimana watak absurdnya, social media seolah-olah membuang batas partisi tersebut menjadi sesuatu yang tidak jelas dan acak. Siapa yang menindih siapa, tetap masih bisa diperdebatkan. Tapi, yang jelas adalah: keduanya bergerak sebagai bagian dari sebuah keramaian, alias: part of the crowd!\r\n\r\nStatus influencer, mampu menunjukan sebuah gejala: bagaimana mereka mengatur kendali. Perannya seperti hegemoni yang bergerak. Bagaimana mereka mampu mendominasi jalannya rotasi, dan nyempilin standar-standar baru ke dalam otak orang-orang yang berada di bawah dominasinya.\r\n\r\nDari pola acak tersebut, hegemoni di dalamnya menjadi berlapis-lapis, dan saling tumpang-tindih. Figur-figur dominan ini memiliki komunitas tertentu yang mereka kuasai. Lihat bagaimana Lambe Turah mampu menjaring fanbase besar dengan konten-konten gosip mereka. Atau bagaimana Awkarin, tetap laku keras sebagai endorser meskipun dirinya berada dalam kutub bad influencer.\r\n\r\nMasing-masing influencer punya kekuasaan yang berbeda-beda. Kekuasaannya itu berbentuk komunitas yang terdiri dari akun-akun, terlepas apakah akun-akun itu merupakan pengikut yang masuk dalam kolom followers atau pengikut bayangan, alias stalker kalau urat kepo-nya tiba-tiba kumat.\r\n\r\nSementara pebisnis atau marketer yang ingin menggunakan jasa influencer sebagai alat untuk mempromosikan merek mereka, biasanya akan melakukan pemetaan terlebih dahulu sebelum menentukan figur mana yang bakal mereka gunakan. Jasa endorse adalah jembatan bagi sebuah merek bisnis untuk menyentuh pangsa pasar mereka yang berada dalam komunitas jajahan sang figur.\r\n\r\nHegemoni sang figur itulah yang sebenarnya dimanfaatkan oleh para penjual sebagai alat jualan. Hari ini, definisi influencer itu sendiri akhirnya dipertemukan dengan cabang baru, yaitu influencer yang lebih mirip sebagai agen-agen elite yang ditugaskan; apa lagi kalau bukan menjadi elite sales?\r\n\r\nTren endorse adalah titik inti dari istilah influencer itu mengalami pergeseran makna. Apakah arti dari istilah sekeren itu hanya sebatas “kemampuan mempengaruhi orang untuk membeli sebuah produk” saja? Pemahaman tersebut mungkin lazim di era sekarang, yang dianggap aneh justru ketika kita menyamakan derajat influencer dengan para online seller.\r\n\r\nLalu, seperti apa orang yang berhak menyandang status tersebut? Siapapun bisa sebenarnya. Setiap manusia punya daya untuk mempengaruhi orang lain walaupun komunitas jajahannya tidak seluas para selebriti media sosial. Bahkan, binatang pun bisa melakukannya. Ingat bagaimana seekor Gurita memprediksi hasil pertandingan Piala Dunia?\r\n\r\nLewat kolomnya, Andreas Maryoto memberikan salah satu contoh upaya memprovokasi. Paling tidak, mempengaruhi pembaca untuk melihat sudut pandang baru tentang influencer lewat kacamata rekan-rekannya yang berkecimpung di bidang kehumasan. Begitu juga dengan unggahan curahan hati para pebisnis yang mengeluhkan pengalaman kerja samanya dengan─orang-orang yang mengaku sebagai─influencer juga merupakan upaya yang sama.\r\n\r\nBoleh dibilang, orang-orang yang disebut sebagai influencer itu adalah pendusta. Eksklusivitas dari status tersebut hanyalah kebohongan yang terdengar lucu. Kita semua adalah influencer itu sendiri. Yang membedakan hanya seberapa berpengaruhnya kita dengan seleb-seleb terkenal itu. Dengan kata lain, cuma kalah terkenal dan kalah jumlah followers.\r\n\r\nPopularitas seharusnya bukan penentu seseorang dapat dikatakan influencer. Tapi, yang terjadi hari ini adalah istilah itu mengerucut menjadi sebuah tanda yang seolah-olah eksklusif. Kenapa tidak sekalian saja kita sebut status itu sebagai nabi? Sama-sama mempengaruhi, sama-sama punya daya, dan sama-sama punya umat pengikut, bukan? Beda, tapi mirip-mirip.\r\n\r\nKepentingan promosi agaknya sangat mendorong istilah itu berada dalam lingkaran yang khas seperti sekarang ini. Jumlah follower dan luas jajahan kini menjadi parameter mutlak seseorang disebut sebagai influencer. Hal itu seakan menanggalkan alasan-alasan lain. Sejauh ini, status tersebut seperti tidak membawa aura kapasitas lain selain menjadi alat jual semata.\r\n\r\nSebagai akun yang mengantongi parameter mutlak tadi, dengan begitu mudahnya mengklaim diri sebagai influencer, kemudian membuka jasa endorse untuk mengeluarkan testimoni-testimoni. Pola ini seakan mengumumkan bahwa influencer, tak lain hanyalah status atau profesi eksklusif yang merendahkan eksklusivitasnya sendiri. Budak-budak testimoni yang kesaksiannya dibayar oleh pemodal.\r\n\r\nPengaruh mereka memang bisa dihitung. Tapi, apabila pengaruhnya hanya berkutat pada statistik penjualan saja, apa bedanya dengan sales person biasa? Bagaimana status ini bisa begitu digdaya ketika perannya hari ini pun sebenarnya monoton dan biasa saja? Status influencer seolah menjadi penamaan makna yang membedakan sesuatu yang sama.\r\n\r\nAlih-alih saintifik, Influencer kita terima secara takjub hanya karena datang lewat medium yang baru. Dunia maya mengorbitkan artis-artisnya lewat kepolosan fantasi para penggunanya. Semakin individu-individu elit itu dielukan, semakin tinggi pula area dan jumlah jajahannya. Dan, dari situlah posisi tawar mereka menguat, alias lebih mahal.\r\n\r\nKetika sadar bahwa dirinya berada di level itu, pengaruh-pengaruh yang dimiliki mulai (cenderung) diobral. Status elit, yang tidak disadari pula, ikut terjajah oleh iming-iming. Hegemoni yang datang dari ketenaran, diinjak lagi oleh hegemoni lain berbentuk perjanjian transaksional yang melibatkan pundi-pundi upah.\r\n\r\nDi atas langit masih ada lapisan langit. Kata-kata itu mungkin pantas dijadikan antitesis bagaimana penyematan status tersebut mestinya tidak seprestis yang ada sekarang. Tapi, pengertiannya itu sudah begitu terlanjur. Kita hanya tinggal memilih; antara meluruskan istilah-istilah─seperti perjuangan Ivan Lanin, sang influencer tata bahasa─itu, atau menunggu ‘rezim status’ tersebut melakukan blunder, sehingga mengalami kejatuhan di masa mendatang.\r\n\r\nDi tengah arus kedigdayaannya para selebritis medsos merias dirinya sebagai influencer saat ini, keluh-kesah yang diunggah para pemegang merek di sudut-sudut pelosok lain dunia maya bisa jadi merupakan tanda-tanda kejatuhan tersebut. Gelombang anti-influencer baru-baru ini mulai terjadi, dan ini merupakan isu yang tentu akan berdampak bagi kelangsungan status para budak-budak testimoni itu.\r\n\r\nArus awareness para audiens (netizen kelas bawah) yang menganggap testimoni-testimoni itu adalah testimoni palsu, sekiranya bakal menguatkan gelombang itu. Mungkin, hari ini status itu masih tetap eksklusif, dan berpotensi sebagai ladang uang. Tapi, ketika seorang budak dianggap tidak lagi memberikan ‘sesuatu’ bagi tuannya, mereka bakal dimerdekakan. Kau tahu maksudnya kan, Esmeralda-chan?\r\n\r\nPaling tidak, kemerdekaan itu bisa berupa runtuhnya pamor “influencer” itu sebagai diksi yang begitu superior. Kemerdekaan yang meruntuhkan sisi puitisnya menjadi sesuatu yang tak lebih dari “gembel generasi baru”, yang bakal mulai mengemis di emperan toko-toko virtual sambil mengobral kata-kata mutiara yang kurang lebihnya berbunyi: \"Berikan saya produk anda secara gratis, dan sebagai gantinya saya akan mempromosikannya lewat Instagram.” Sungguh, proposal yang tidak lebih baik dari para promotor acara─pemburu sponsor.\r\n\r\nSeperti layaknya mobil dengan motor, sebutan bagi keduanya berbeda, tapi konsep mereka tetaplah sama, yakni: kendaraan. Apa-apa yang terkesan eksklusif dan prestise, memang selalu menggelikan, ya? Semoga, tulisan ini tidak kalah provokatifnya dibanding selebaran testimoni produk-produk peninggi badan di Instagram.', '2019-12-19 14:13:07', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `status`) VALUES
(1, 'baim', 'ganteng', 1),
(2, 'admin', 'admin', 0);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `blog`
--
ALTER TABLE `blog`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `blog`
--
ALTER TABLE `blog`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
