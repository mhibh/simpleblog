<?php

$login = false;
if (isset($_SESSION['Username'])) {
  $login = true;
};

?>

<!DOCTYPE html>
<html lang="en">

<head>

  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="view/css/bootstrap.css">

  <!-- Style CSS -->
  <link rel="stylesheet" href="view/css/style.css">
  <title>Simple Blog</title>
</head>

<body>

  <nav class="navbar navbar-expand-lg navbar-light bg-transparent">
    <div class="container">
      <a class="navbar-brand" href="./index.php">Blog</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item active">
            <a class="nav-link" href="./index.php">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="./all-article.php">All Articles</a>
          </li>

          <?php if ($login) : ?>
            <li class="nav-item">
              <a class="nav-link" href="./tambah.php">Tambah Data</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="./logout.php">logout</a>
            </li>
          <?php else : ?>
            <li class="nav-item">
              <a class="nav-link" href="./login.php">login</a>
            </li>
          <?php endif ?>
        </ul>
      </div>
    </div>
  </nav>