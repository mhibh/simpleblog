<?php require_once "core/init.php";
require_once "view/header.php";

$articles = tampilkan();

?>


<div class="container mt-5">
    <div class="row">
        <?php while ($row =  mysqli_fetch_assoc($articles)) : ?>
            <div class="col-sm-4">
                <div class="card text-center p-0">
                    <div class="card-body">
                        <img src="view/asset/icon/memphiz_title.svg" class="img-fluid" width="30">
                        <h5 class="card-title mt-1"><?= $row['title']; ?></h5>
                        <p class="card-text"><?= $row['time']; ?></p>
                        <hr>
                    </div>
                </div>
            </div>
        <?php endwhile;   ?>
    </div>

</div>

<?php require_once "view/footer.php"; ?>