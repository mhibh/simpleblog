<?php require_once "core/init.php";



if (isset($_SESSION['Username'])) {
    header('Location: index.php');
} else {
    $error = '';


    if (isset($_POST['submit'])) {

        $username = $_POST['username'];
        $password = $_POST['password'];

        if (!empty(trim($username)) && !empty($password)) {

            if (check_data($username, $password)) {
                $_SESSION['Username'] = $username;
                header('Location: index.php');
            } else {
                $error = 'ada masalah ketika login';
            };
        } else {
            $error =  'Username dan data wajib diisi';
        }
    }

    require_once "view/header.php";
}
?>



<div class="container">

    <form action="" method="post">
        <div class="form-group">

            <label for="username">Username</label>
            <input type="text" class="form-control" name="username" id="">

            <label for="password">Password</label>
            <input type="password" class="form-control" name="password" id="">
            <div id="error"> <?= $error ?> </div>

            <input type="submit" name="submit" class="btn btn-primary mt-3" value="Submit">



        </div>

    </form>
</div>









<?php


require_once "view/footer.php";


?>